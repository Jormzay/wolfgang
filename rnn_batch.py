import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

# Utility method to help creation of model from dictionary args
# Useful when wanting to create a model from saved pickle file
def make_batch_lstm(args, device=None):
    if 'embedding_dim' not in args:
        args['embedding_dim'] = 100
    if 'num_layers' not in args:
        args['num_layers'] = 2
    if 'hidden_size' not in args:
        args['hidden_size'] = 256
    if 'dropout' not in args:
        args['dropout'] = 0.2
    if 'padding_idx' not in args:
        args['padding_idx'] = 0
    
    return BatchLSTMmodel(**args).to(device=device), args

class BatchLSTMmodel(nn.Module):
    def __init__(self, dictionary_size, embedding_dim=100, num_layers=2,
            hidden_size=256, dropout=0.2, padding_idx=0):
        """
        Creates RNN with Embedder and LSTM, Dropout, and Linear layers.

        Args:
            dictionary_size (int): number of unique characters in input sequences
            hidden_size (int): size of first hidden layer, power of 2'
            batch_size (int): batch size of input batches
            padding_idx (int): index used for padding sequences in input batch
        """
        super().__init__()
        self.dictionary_size = dictionary_size
        self.embedding_dim = embedding_dim
        self.num_layers = num_layers
        self.hidden_size = hidden_size
        self.dropout = dropout
        self.padding_idx = padding_idx

        # Embed padded sequences of characters to vectors of size hidden_size
        self.embedding = nn.Embedding(
            num_embeddings=self.dictionary_size,
            embedding_dim=self.embedding_dim,
            padding_idx=self.padding_idx
        )

        # LSTM layer; batch_size as first dimension of input
        self.lstm = nn.LSTM(
            input_size=self.embedding_dim,
            hidden_size=self.hidden_size,
            num_layers=self.num_layers,
            dropout=self.dropout, # applies dropout to all layers except last!
            batch_first=True
        )

        # Dropout applied to the last layer as well (is this needed?)
        self.dropout_layer = nn.Dropout(p=self.dropout)

        # Linear layer: map results to pseudoprobabilities of each unique
        # character (dictionary_size different characters) being the next one
        self.linear = nn.Linear(self.hidden_size, self.dictionary_size)

        # Softmax layer to get actual probabilities, log to allow use of
        # NLLLoss criterion for training); dim=1 to get probabilities for each
        # element in the sequence separately
        self.log_softmax = nn.LogSoftmax(dim=1)

    def forward(self, input_batch, input_batch_lengths, device=None):
        """
        Args:
            input_batch (tensor): Tensor of sequences of words (word indices) of
                the input sentences. The shape is [batch_size, seq_length].

        Returns:
            log_softmax_out (tensor): log probabilities of characters in sequence
        """
        # Save size of input for reshaping back later
        batch_size, seq_length = input_batch.size()

        # 1. Embed batch of sequences
        embedded = self.embedding(input_batch)
        # Size embedded: [batch_size, seq_len, embedding_size]

        # 2. LSTM layer
        # Preprocessing: pack_padded_sequence for LSTM
        packed_padded = pack_padded_sequence(embedded,
                                        input_batch_lengths,
                                        batch_first=True,
                                        enforce_sorted=False)

        # Process using LSTM layer; output dimensions
        # (batch_size, seq_length, hidden_size)
        lstm_out, self.hidden = self.lstm(packed_padded, self.hidden)

        # Postprocessing: pad_packed_sequence
        unpacked_padded, _ = pad_packed_sequence(lstm_out,
                                            total_length=seq_length,
                                            batch_first=True)
        # Size unpacked_padded: [batch_size, seq_len, hidden_size]

        # 3. Final dropout layer
        dropout_out = self.dropout_layer(unpacked_padded)
        # Size dropout_out: [batch_size, seq_len, hidden_size]

        # 4. Linear layer: map to pseudoprobabilities of next indices
        # Reshape to fit into linear layer
        linear_in = dropout_out.view(-1, dropout_out.shape[2])
        # Size linear_in: [batch_size*seq_len, hidden_size]

        # Pass through linear layer
        linear_out = self.linear(linear_in)
        # Size linear_out: [batch_size*seq_len, dict_size]

        # 5. LogSoftMax activation (map to log probabilities)
        log_softmax_out = self.log_softmax(linear_out)
        # Size log_softmax_out: [batch_size*seq_len, dict_size]

        return log_softmax_out

    def init_hidden(self, batch_size, device=None):
        """
        Initialize hidden state for LSTM layers for batches of correct size.

        Arguments:
            device (optional): device to put tensors on
        """
        # shape requirements based on LSTM definition in pytorch
        hidden_shape = (self.num_layers, batch_size, self.hidden_size)

        # Init the LSTM hidden state.
        # The first item in the tuple is h_0: "the initial hidden state for
        #   each element in the batch."
        # The second item in the tuple is c_0: "initial cell state for each
        #   element in the batch"
        self.hidden = (
            torch.zeros(hidden_shape, device=device),
            torch.zeros(hidden_shape, device=device)
        )
