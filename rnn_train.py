# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 10:25:26 2019

@author: Jonathan
"""
import numpy as np
import random
import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F

def makeTargetData(input_sequence):
    
    '''
    Creates target sequence with correct structure for NLLLoss() for given input sequence 
    
    Arguments:
    
        input_sequence (Tensor): Input sequence
       
    Returns:
        
        target_sequence (Tensor): Target sequence of same size as input
    
    '''
    #convert tensor to python list
    data = input_sequence.tolist() 
    
    #duplicate the last character (always a linebreak) at end of sequence, 
    #making the target output for the last character the same as the input
    data.append(data[-1])
    
    #create target_sequence list
    target_sequence = np.zeros((input_sequence.size(0)))
    
    for i in range(len(data) - 1):
        
        #make the next character in the input_sequence the target for the current one
        target_sequence[i] = data[i + 1]
    
    #Return target_sequence as tensor with datatype long
    return torch.Tensor(target_sequence).long() 



def train(data, model, dict_size, epochs=1, verbose=True, device=None):
    
    '''
    Trains given model with given data using NLLLoss and Adam optimizer.
    
    Arguments:
    
        data (List of Tensors): List of index-encoded songs
        
        model (nn.Module instance): Model to be trained. Given input sequence of size (seq_len), expects output of size (seq_len, 1, dict_size)            
        
        dict_size (int): Size of dictionary that sequence indices are referring to        
    
        epochs (int): Number of times to loop throughthe dataset
        
        verbose (boolean): Satisfying printouts
    
    '''
    #set up NLL-Loss and Adam optimizer with some nice learning rate
    criterion = nn.NLLLoss()
    optimizer = optim.Adam(model.parameters(), lr = 0.01)
    
    for epoch in range(epochs):        

        # Keep track of song number (for verbose learning)
        if verbose:
            song_number = 0
        
        for input_seq in data:
            
            #move next input sequence to correct device, if defined
            if device is not None:
                input_seq = input_seq.to(device)
            
            #reset optimizer
            optimizer.zero_grad()
            
            #initialize hidden state for model (as tensor of zeros)
            model.init_hidden(device)
            
            #create target sequence
            target_seq = makeTargetData(input_seq)
            
            #move next target sequence to correct device, if defined
            if device is not None:
                target_seq = target_seq.to(device)
            
            #make predicton
            output_seq = model.forward(input_seq).view(input_seq.size(0), dict_size)
            
            #compute loss, backpropagate and update weights
            loss = criterion(output_seq, target_seq)
            loss.backward()           
            optimizer.step()
            
            if verbose:
                # Increment song number
                song_number += 1
                print(("Epoch: " + str(epoch+1) +
                       ", song number: " + str(song_number) +
                       ", loss: " + str(loss.item())))
        
        #shuffle training data between epochs
        random.shuffle(data)
        
            
