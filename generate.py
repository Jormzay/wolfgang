# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 00:22:58 2019

@author: Jonathan
"""
import torch
import numpy as np


def generate_song(model, dictionary, song_length = 250, device=None):
    
    """
            Args:
                model (nn.Module):      Trained instance of model that predicts next character in sequence.
                
                dictionary (string):    List of unique characters in dictionary
                
                
            Returns:
                song (string): generated string
    """
    
    #set first index as the one for 'K'
    current_index = dictionary.index('K') 
    song = ''
    
    
    for _ in range(song_length):
        
        #append the character to song
        song += dictionary[current_index]
        
        #initialize hidden
        model.init_hidden(device)

        #generate prediction based on latest character and convert it to python list (--> becomes list wihin list within list)
        pred = model.forward(torch.Tensor([current_index]).to(device).long()).tolist()[0][0]
        
        #select character stochastically from the predicted distribution
        current_index = np.random.choice(range(len(dictionary)), size = 1, p = ((np.exp(pred))/sum(np.exp(pred))))[0]
        
        #in case the model has decided to generate an empty line (two linebreaks in a row), 
        #representing the end of a song in the training data, stop generation.
        if dictionary[current_index] == '\n' and song[-1] == '\n':
            break

    return song