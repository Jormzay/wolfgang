import os, sys, re

# -----------------------------------------
# -- 1. some helper methods to read files...
# -----------------------------------------

def get_all_song_paths(path, extension='.abc'):
    """
    For some directory path, and some extension:
        Return a list of all the paths (recursively)
        To all the files with the correct extension, in
        dir or some subfolder of dir :)

        alternatively, if it's a file path. Just return that.
    """
    if path.endswith(extension):
        return [path]

    files = []
    for d, _, found_files in os.walk(path):
        for f in found_files:
            if f.endswith(extension):
                files.append("%s/%s" % (d, f))
    return files


def file_to_string(filepath):
    """
    Read all text from one file, return string.
    """
    # some files contain improper characters
    # so we read in as bytes and decode, while ignoring those chars
    with open(filepath, 'rb') as f:
        text = f.read().decode('utf-8', errors='ignore')
    return text


def clean_song(song_text, remove_headers=True):
    """
    Clean a single song file. Mostly the idea here is to
      keep parts that encode actual music/tune data and
      remove cosmetic parts such as comments or annotations.
      
      Additionally, this method can be used as preprocessing of
      a song before passing it to pyabc.Tune(abc=clean_song) so
      that pyabc has a higher chance of parsing the data...
    """
    song_text = song_text.strip()

    # 1. Fix possibly missing reference number
    if len(song_text) > 0 and song_text[0] != 'X':
        song_text = 'X: 1\n' + song_text

    # 2. remove abc comments
    song_text = re.sub("\n? *%[^\n]+", "", song_text)
    
    
    # 3. remove abc headers except for Key (K) and Measure (M)
    # also switch their places to maintain expected structure of training data
    measure_str = ''
    measure = re.search('\nM:(.+?)\n', song_text)
    
    if measure is not None:
        measure_str = measure.group(0)
    
    if song_text.count("\nK:") > 0:
        key = re.search('\nK:(.+?)\n', song_text)
        
        if key == None:
            return None
        
        if remove_headers:
            song_text = song_text[key.end():]
            song_text = key.group(0)[1:] + measure_str[1:] + song_text
            
            
    else:
        return None
    
    # 4. remove abc linebreaks
    song_text = song_text.replace("\\\n", "\n")
    
    # 5. remove parts (P:) and voice (V:) and lyrics (w:)
    # Note: these appear in the tune part, not the header.
    # that's why we remove them separately here...
    song_text = re.sub("\n[PVw]:[^\n]+", "", song_text)
    
    # 7. Remove extra whitespace
    song_text = song_text.strip()

    if not song_text:
        return None

    return song_text + "\n\n"


def read_songs(paths, len_min, len_max):
    """
    For a list of multiple filepaths to song files:
        Read all of them into a large array where each item will
        be the abc notation of that one song only.
        Note: some files might have multiple songs (split by \n\n)
    
    Args:
        paths: list of individual .abc file absolute paths
        len_min: output only songs that are at least this long (incl.)
        len_max: output only songs that are less than this in length (incl.)
    """
    songs_array = []
    unique_chars = set()

    total_count = 0
    pruned_count = 0
    pruner = lambda song: len(song) >= len_min and len(song) <= len_max

    # 1. read all songs, clean them, and put them in a string array
    for f in paths:
        # songs end in a double newline \n\n
        text = file_to_string(f)
        # replace eol
        text = text.replace('\r\n', '\n')
        # individual songs marked by double newline
        songs = text.split("\n\n")
        
        # remove noise characters --> "clean" songs
        clean_songs = list(filter(None, map(clean_song, songs)))
        total_count += len(clean_songs)
        pruned_count += len(clean_songs)

        # prune songs that are too short or too long
        good_length_songs = list(filter(pruner, clean_songs))
        pruned_count -= len(good_length_songs)

        songs_array += good_length_songs
    
    info_str = "Power loader: loaded %d songs. (%d were pruned based on length)"
    print(info_str % (total_count - pruned_count, pruned_count))

    # 2. collect all unique characters we found
    for song in songs_array:
        for c in song:
            unique_chars.add(c)
    
    # 3. encode characters as numbers
    unique_chars.remove('K')
    unique_chars = ['K'] + list(unique_chars) # make sure K is first...
    unique_chars = ['Ö'] + list(unique_chars) # make index 0 a character not included in ABC notation
    encoding = dict((c, i) for i,c in enumerate(unique_chars))
    encoded_songs = [list(map(encoding.get, song)) for song in songs_array]
    
    return unique_chars, encoded_songs


# -----------------------------------------
# -- 2. bring it all together in one method
# -----------------------------------------

def parse(path, len_min=20, len_max=200):
    """
    Input: directory with .abc files OR path to a single abc file
    Output: Tuple
        songs: array of all songs in abc notation
        unique_characters: set of individual characters
    """
    paths = get_all_song_paths(path)
    unique_chars, encoded_songs = read_songs(paths, len_min, len_max)
    return unique_chars, encoded_songs
