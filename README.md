# Wolfgang

> "I’m a hustler, baby; I sell water to a well!"
> - W.A. Mozart

___

## Batch LSTM Instructions

**Train new model:**

- Works on `cuda` or `cpu`
- Models will be saved in the `models` folder

```bash
# train new model and save in models/mozart.pth
python train_batch.py mozart
```

**Generate songs:**

- Songs are saved in the `billboard_hot_100` folder
- Modify the script to change model

```bash
# Saves song to billboard_hot_100/wonderwall.abc
python generate_batch.py wonderwall
```

## Regular LSTM Instructions

Call Joni R. at +358 123456789
