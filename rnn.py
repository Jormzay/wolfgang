# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 09:24:17 2019

@author: cosmo
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

class LSTMmodel(nn.Module):
    def __init__(self, dictionary_size, hidden_size):
        """
        Creates RNN with Embedder and LSTM, Dropout, and Linear layers.

        Args:
            dictionary_size (int): number of unique characters in input sequences
            hidden_size (int): size of first hidden layer, power of 2
        """
        super().__init__()
        self.dictionary_size = dictionary_size
        self.hidden_size = hidden_size
        
        # Embed characters to vectors of size hidden_size
        self.embedding = nn.Embedding(dictionary_size, hidden_size)
        # LSTM layer
        self.lstm = nn.LSTM(input_size=hidden_size,
                            hidden_size=hidden_size)
        # Dropout layer at end of LSTM layer
        self.dropout = nn.Dropout(p=0.2)
        # Linear layer: map results to pseudoprobabilities of each unique
        # character (dictionary_size different characters) being the next one
        self.linear = nn.Linear(hidden_size, dictionary_size)
        # Softmax layer to get actual probabilities, log to allow use of
        # NLLLoss criterion for training); dim=2 to get probabilities for each
        # element in the sequence separately
        self.log_softmax = nn.LogSoftmax(dim=2)

    def forward(self, input_seq):
        """
        Args:
            input_seq (tensor):  Tensor of words (word indices) of the input sentence. The shape is
                               [seq_length, batch_size] with batch_size = 1.

        Returns:
            log_softmax_out (tensor): log probabilities of characters in sequence
        """
        # Embed sequence
        embedded = self.embedding(input_seq)
        # First LSTM layer; reshape input to match definition
        # (seq_len, batch, input_size)
        lstm_out, self.hidden = self.lstm(embedded.view(len(input_seq), 1, -1),
                                          self.hidden)
        # Dropout layer
        dropout_out = self.dropout(lstm_out)
        # Linear layer: map to pseudoprobabilities of next characters (indices)
        linear_out = self.linear(dropout_out)
        # LogSoftMax: map to log of probabilities of next characters
        log_softmax_out = self.log_softmax(linear_out)

        return log_softmax_out
    
    def init_hidden(self, device=None):
        """
        Initialize hidden state for LSTM layers.
        
        Arguments:
            device: device to put tensors on
        """
        if device is None:
            self.hidden = (torch.zeros(1, 1, self.hidden_size),
                           torch.zeros(1, 1, self.hidden_size))
        else:
            self.hidden = (torch.zeros(1, 1, self.hidden_size).to(device),
                           torch.zeros(1, 1, self.hidden_size).to(device))










































