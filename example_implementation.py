# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 20:53:14 2019

@author: Jonathan
"""

import torch
import numpy as np
import generate
from utils import power_loader
import rnn
import rnn_train
import time

# Choose device to use for training
#device = torch.device('cpu')
device = torch.device('cuda:0')

#load all abc songs from a file or folder
dictionary, songlist_indexed = power_loader.parse('data/lotro2/')

#convert songs to tensors of type long
songlist_tensor = []
for i in songlist_indexed:
    songlist_tensor.append(torch.Tensor(i).long())

#set up LSTM model
mozart = rnn.LSTMmodel(len(dictionary), hidden_size=256)
# Move model to correct device
mozart.to(device)

#train the model on the dataset
start_time = time.time()
rnn_train.train(songlist_tensor, mozart, len(dictionary), epochs=10,
                verbose=True, device=device)
end_time = time.time()
print("Training time: " + str(end_time - start_time))

# save model weights
#torch.save(mozart.state_dict(), "mozart.pth")

#song length
SONG_LENGTH = 500

#generate song
song = generate.generate_song(mozart, dictionary, SONG_LENGTH, device)
