import os, sys
import numpy as np
import torch
from utils import pyabc
from rnn_batch import make_batch_lstm

# -----------------------------------
# Generate songs from batch LSTM models
#
# Call this script like this
# python generate_batch.py hit_song_name
#
# It will save the new song in the billboard_hot_100 folder
# (edit this file to change default model...)
# -----------------------------------

device = torch.device('cpu')

# ----------------------------------------
# ------- 1. some helper methods ---------
# ----------------------------------------

def print_info(saved_dict):
    """The batch training script puts a lot of extra info in the saved .pth
    files. This method prints out some cool facts!
    """
    def pretty_print(d, indent=2):
        for key, value in d.items():
            print(' ' * indent + str(key) + ": " + str(value))
    
    print("-" * 30)
    print("Artist name: %s" % saved_dict['artist'])
    print("-" * 30)

    print("Training info:")
    pretty_print(saved_dict['training_info'])

    print("\nModel architecture:")
    pretty_print(saved_dict['model_args'])
    print("-" * 30)


def load_model(path, device=None):
    """From a model that was saved by the train_batch.py script, load the model
    along with extra info!

    Call this method to load a saved model and generate hits ;)

    Returns:
        pytorch model object and dictionary (list)
    """
    data = torch.load(model_path, map_location=device)
    dictionary = data['dict']

    model, _ = make_batch_lstm(data['model_args'], device=device)
    model.load_state_dict(data['model'])

    # Print info about the model...
    print_info(data)

    artist = data['artist']
    return model, dictionary, artist

def generate_song(model, dictionary, artist, device, verbose=True):
    """From a loaded (batch) model and corresponding dictionary, generate
    a single song and return its abc-string.

    Optionally specify a maximum length for the songs generated.

    Note: this method does NOT guarantee that the song is playable.
    """
    dict_size = len(dictionary)
    padding_index = 0
    eol_index = dictionary.index('\n')
    current_index = dictionary.index('K') # K as start index

    # good models won't have a problem with max_length
    max_length = 500

    # Initialize song with only padding
    song = torch.zeros([max_length], dtype=torch.long, device=device) + padding_index
    lengths = torch.Tensor([1]).detach()

    model.init_hidden(1, device=device)

    for i in range(max_length):
        song[i] = current_index

        # Songs end in a double newline
        if i > 0 and song[i-1] == eol_index and song[i] == eol_index:
            break
        
        log_probs = model.forward(song[i].view(1, 1), lengths).squeeze()

        # Torch doesn't have random choice... :(
        log_probs_np = log_probs.detach().numpy()
        probs = np.exp(log_probs_np)

        # check that probs sum to one, otherwise it's faulty...
        if verbose and not np.isclose(np.sum(probs), 1.0):
            print("Probabilities didn't sum to one???")

        current_index = np.random.choice(dict_size, p=probs)
    
    song_length = i+1
    if verbose and song_length == max_length:
        print("Warning: max length of song was reached!")

    # strip padding & convert to abc string
    song = song[:song_length].tolist()
    song_abc = "X:1\nT:Anyway, here's Wonderwall\nC:%s\n" % artist
    song_abc += "".join([dictionary[i] for i in song])

    return song_abc, song_length

def generate_and_validate(model, dictionary, artist, device=None):
    """Sometimes Mozart is drunk and composes bad songs. This method asks him
    to generate many songs until he makes one that is valid..!
    """
    max_attempts = 100
    print("\nComposing... (max %d attempts)" % max_attempts)
    for i in range(1, max_attempts+1):
        abc, length = generate_song(model, dictionary, artist, device, verbose=False)
        try:
            # Try parsing generated song
            pyabc.Tune(abc=abc)
            print("Generated good song on attempt %d!" % i)
            return abc, length
        except:
            pass
    
    print("Couldn't generate a song in %d attempts :(" % max_attempts)
    return None, None


# --------------------------------------------
# ------ 2. Actually generate a song ---------
# --------------------------------------------

# decide model + song name and path
model_path = 'models/mozart-1337.pth'
song_name = 'wonderwall' if len(sys.argv) == 1 else sys.argv[1]
song_path = 'billboard_hot_100/%s.abc' % song_name

# check that the song didn't exist already
if os.path.exists(song_path):
    print("Error: Song %s already made! Pick a new name!" % song_name)
    sys.exit(1)

# load model and make a song!
model, dictionary, artist = load_model(model_path, device=device)
song_abc, song_length = generate_and_validate(model, dictionary, artist,
                                        device=device)

# print info, did we succeed in making a valid song?
if song_abc:
    with open(song_path, 'w+', encoding='utf8') as f:
        f.write(song_abc)
    print("\nPump up the base!!!")
    print("A new hit song (len=%d) is now available at:" % song_length)
    print("  %s\n" % song_path)
else:
    print("\nNo song was produced...")
    print("Maybe your model sucks?\n")
