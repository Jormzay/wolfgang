import os
import sys
import torch
from torch import nn
import numpy as np
from utils.power_loader import parse
from rnn_batch import make_batch_lstm
from time import time

# -----------------------------------
# Train a batch LSTM model
#
# Call this script like this (specify name of model!)
# python train_batch.py model_name
#
# It will save the new model in models/model_name.pth
# -----------------------------------

# choose device cpu/cuda to train on
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
print('Using device:', device)

def load_and_pad(data_dir='data', len_min=150, len_max=300, device=None):
    """Load data from .abc files into X (training data) and y (targets)

    Returns:
        X: padded Tensor(s) or list of Tensors
        lengths: song lengths for each sample in X
        y: same shape as X, targets to predict
    """
    # load all .abc files into a python list, where each item item is a list of
    # indexed tokens in the abc tune file. The dictionary is the mapping from
    # string tokens to indicies eg. "K" -> 1, etc.
    dictionary, songs_indexed = parse(data_dir, len_min=len_min, len_max=len_max)
    n_samples = len(songs_indexed)

    lengths = [len(song) for song in songs_indexed]
    len_min, len_med, len_max = min(lengths), np.median(lengths), max(lengths)

    # X will be a right-padded torch Tensor
    pad_token = 0
    X = torch.zeros([n_samples, len_max], dtype=torch.long, device=device) + pad_token
    for i, song in enumerate(songs_indexed):
        X[i,:lengths[i]] = torch.Tensor(song)

    # the target of a token will always be the next token..!
    y = torch.cat((X[:,1:], X[:,-1:]), 1)
    assert y.shape == X.shape
    
    X_lengths = torch.Tensor(lengths).detach().requires_grad_(False)
    return X, y, X_lengths, dictionary

def train(data, args, lr=0.01, batch_size=16, epochs=1, device=None):
    print("Started training model")
    t_start = time()
    last_loss = None # set during training...

    X, y, X_lengths, dictionary = data
    model, args = make_batch_lstm(args, device=device)

    n_samples = X.size(0)
    n_batches = n_samples // batch_size
    pad_token = 0

    criterion = nn.NLLLoss(ignore_index=pad_token)
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    losses_list = []

    for epoch in range(1, epochs+1):
        inds = torch.randperm(n_samples, device=device)
        losses = np.zeros(n_batches)
        e_start = time()
        
        for batch_index in range(n_batches):
            curr_inds = inds[batch_index*batch_size:(batch_index+1)*batch_size]
            X_batch, y_batch = X[curr_inds], y[curr_inds]
            X_len_batch = X_lengths[curr_inds]

            # Init hidden state. Done once for batch sequence (song batch)
            model.init_hidden(batch_size, device=device)
            
            # reset optimizer
            optimizer.zero_grad()
            
            # make predicton
            y_batch_pred = model.forward(X_batch, X_len_batch, device=device)
            
            # compute loss, backpropagate and update weights
            loss = criterion(y_batch_pred, y_batch.flatten())
            losses[batch_index] = loss

            loss.backward()           
            optimizer.step()
        
        epoch_info_str = "Epoch %d/%d - loss: %.7f, time: %.3fs"
        last_loss = losses.mean()
        losses_list.append(last_loss)
        print(epoch_info_str % (epoch, epochs, last_loss, time() - e_start))
    
    training_time = time() - t_start

    # general info about the training process
    # not used anywhere but can be saved and reviewed :)
    training_info = {
        'epochs': epochs,
        'lr': lr,
        'loss': last_loss,
        'losses': losses_list,
        'batch_size': batch_size,
        'n_songs': n_samples,
        'min_length': str(int(torch.min(X_lengths).detach().numpy())),
        'max_length': str(int(torch.max(X_lengths).detach().numpy())),
        'time_seconds': int(training_time),
        'device': "cpu" if not device else str(device)
    }

    print("Finished training in %.3fs" % training_time)
    return model, args, dictionary, training_info


# ------------------------------------------------------
# ------------ actually run training! ------------------
# ------------------------------------------------------

# path/name of the new model we're training now!
model_name = 'mozart' if len(sys.argv) == 1 else sys.argv[1]
model_path = './models/%s.pth' % model_name

if os.path.exists(model_path):
    print("Model %s already exists! Choose a different name..." % model_name)
    sys.exit(1)

# load data and train model (slow!)
data = load_and_pad(data_dir='data', len_min=150, len_max=500, device=device)

# specify model parameters and train it
args = {'dictionary_size': len(data[3]), 'embedding_dim': 80, 'num_layers':3}
model, full_args, dictionary, ti = train(data, args, batch_size=64, epochs=150, lr=0.005, device=device)

# save both model and dictionary to file
torch.save({
    'artist': model_name,
    'model': model.state_dict(),
    'model_args': full_args,
    'dict': dictionary,
    'training_info': ti
}, model_path)

print("Success: saved model & dict to '%s'" % model_path)
