X:1
T:Promise Reprise (Lute 2)
M:4/4
L:1/8
Q:1/4=112
K:Bb
V:1

x8| \
x8| \
x8| \
x8|
x8| \
x8| \
[d8-B8-A8-G8-]| \
[d8-B8-A8-G8-]|
[d8-B8-A8-G8-]| \
[d8-B8-A8-G8-]| \
[dBAG][g6-e6-d6-B6-][g-e-d-B-]| \
[g8-e8-d8-B8-]|
[g8-e8-d8-B8-]| \
[g8-e8-d8-B8-]| \
[g2e2d2B2] x6| \
[d8-B8-A8-G8-]|
[d8-B8-A8-G8-]| \
[d8-B8-A8-G8-]| \
[d8-B8-A8-G8-]| \
[dBAG][g6-e6-d6-B6-][g-e-d-B-]|
[g8-e8-d8-B8-]| \
[g8-e8-d8-B8-]| \
[g8-e8-d8-B8-]| \
[g2e2d2B2] x6|
[d8-B8-A8-G8-]| \
[d8-B8-A8-G8-]| \
[d8-B8-A8-G8-]| \
[d8-B8-A8-G8-]|
[dBAG][g6-e6-d6-B6-][g-e-d-B-]| \
[g8-e8-d8-B8-]| \
[g8-e8-d8-B8-]| \
[g8-e8-d8-B8-]|
[g2e2d2B2] x6| \
[d8-B8-A8-G8-]| \
[d8-B8-A8-G8-]| \
[d8-B8-A8-G8-]|
[d8-B8-A8-G8-]| \
[dBAG][g6-e6-d6-B6-][g-e-d-B-]|[g8-e8-d8-B8-]|[g8-e8-d8-B8-]|
[g8-e8-d8-B8-]|[g2e2d2B2]