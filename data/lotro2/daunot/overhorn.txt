X: 1
T: I'ts Not Over (vocals) (by Cervantes of Meneldor)
Z: Daughtry
L: 1/4
Q: 70
K: C

z9/2 ^A/4 ^G/8 z/8 ^A/2 ^G/8 z/8 ^A9/8 z/8 ^A/4 ^G/8 z/8 ^c/2 ^G3/2
z/4 ^A/8 z/8 ^c3/8 z/8 ^c3/8 z/8 ^G/4 ^G/2 ^A7/8 z/8 ^G/4 ^A/8 z/8
^A/8 z/8 ^G/4 ^A9/8 z/8 ^A3/8 z/8 ^c ^G z/4 ^A/4 ^c/8 z/8 ^c3/8 z/8
[^c5/8z/2] ^A/8 z/8 ^A3/8 z5/8 f/2 ^d/2 ^c3/8 z/8 =c/2 ^c/8 z/8 ^c/2
^A/8 z/8 ^A3/8 z3/8 ^A/8 z/8 f/2 [^d5/8z/2] ^c3/8 z/8 =c/2 ^c/8 z/8
^c/2 ^A/8 z/8 ^A3/8 z9/8 ^A3/8 z/8 ^A/8 z/8 =c9/8 z/8 ^c3/8 z/8 ^A/8
z/8 ^A9/8 z5/8 ^G/4 ^A/8 z/8 =c3/8 z/8 c3/8 z/8 [c3/8z/4] ^G3/4
[^A19/4z9/2] f/4 ^d/4 f/4 ^d/4 f/4 ^d/4 f/2 ^d/4 f/2 ^f/4 =f/4 ^d3/8
z3/8 ^c3/8 z/8 ^c/2 ^g7/8 z/8 f2 z/4 ^c/8 z/8 ^d/4 ^c/4 ^d/4 ^c/4
^d/2 ^c/4 ^d/2 f/4 ^A/2 z/2 ^c3/8 z/8 ^c/2 ^g f ^d7/8 z/8 ^d/4 ^c/4
^d/4 ^c/4 ^d/4 ^c/4 ^d/2 ^c/4 ^d/2 f/8 z/8 f/4 ^d3/8 z7/8 ^c3/8 z/8
^g f3/4 z/4 [^a9/8z] ^g ^f3/4 ^c/8 z/8 ^d/4 ^c/8 z/8 [^d5/8z/2] =f/8
z/8 [^f3/8z/4] [=f5/8z/2] ^c3/8 z/8 ^c/2 [^g9/8z] [=c9/8z] ^A19/8
z53/8 ^A/4 ^G/4 ^A/4 ^A/4 ^G/4 ^A5/8 z3/8 ^G/4 [^A3/8z/4] ^G3/8 z/8
^c3/4 z/4 ^G3/4 z/4 ^G/4 ^c3/8 z/8 ^c3/8 z/8 ^G/8 z/8 ^G3/8 z/8 ^A/2
z3/4 ^A/8 z/8 ^A3/8 z/8 ^A7/8 z3/8 ^A/8 z/8 ^A3/8 z/8 ^A7/8 z7/8 ^A/2
^c/2 ^d/4 ^c/2 ^A/8 z/8 ^A5/8 z3/8 f3/8 z/8 ^d3/8 z/8 ^c3/8 z/8 =c/2
^c/8 z/8 ^c/2 ^A/8 z/8 ^A/2 z/4 ^A/8 z/8 f/2 ^d3/8 z/8 ^c/2 =c/2 ^c/8
z/8 ^c/2 ^A/8 z/8 ^A3/8 z7/8 ^G/8 z/8 ^A/8 z/8 ^A/8 z/8 ^A/8 z/8
=c7/8 z/8 c/8 z/8 ^c/4 ^A/8 z/8 ^A/8 z/8 ^A/4 ^G7/8 z3/8 ^G/4 ^A/4
=c3/8 z/8 c3/8 z/8 ^c3/8 z/8 ^G3/8 z/8 ^A13/8 z33/8 ^G/4 ^A/8 z/8
=c3/8 z/8 c3/8 z/8 c/2 ^G/4 z/8 ^A4 z5/8 f/4 ^d/4 f/4 ^d/4 f/4 ^d/4
f/2 ^d/4 f/2 ^f/4 =f/4 ^d3/8 z3/8 ^c3/8 z/8 ^c/2 ^g7/8 z/8 f2 z/4
^c/8 z/8 ^d/4 ^c/4 ^d/4 ^c/4 ^d/2 ^c/4 ^d/2 f/4 ^A/2 z/2 ^c3/8 z/8
^c/2 ^g f ^d7/8 z/8 ^d/4 ^c/4 ^d/4 ^c/4 ^d/4 ^c/4 ^d/2 ^c/4 ^d/2 f/8
z/8 f/4 ^d3/8 z7/8 ^c3/8 z/8 ^g f3/4 z/4 [^a9/8z] ^g ^f3/4 ^c/8 z/8
^d/4 ^c/8 z/8 [^d5/8z/2] =f/8 z/8 [^f3/8z/4] [=f5/8z/2] ^c3/8 z/8
^c/2 [^g9/8z] [^d13/8z3/2] ^c19/8 z37/4 z11/8 ^c/8 z/8 ^c/2 ^g/2
^c3/4 ^d/4 ^c/2 ^d5/8 f3/8 z3/8 ^c/4 =c/2 ^c7/8 z/4 [^c3/8z/4]
[=c5/8z/2] ^c9/8 z/8 [^A3/8z/4] [=c5/8z/2] ^c5/8 =c/2 ^G/2 F/2 ^A7/8
z/4 ^c/4 =c3/8 z/8 c15/8 z7/8 ^c3/8 z/8 ^c/2 ^g7/8 z/8 f2 z/4 ^c/8
z/8 ^d/4 ^c/4 ^d/4 ^c/4 ^d/2 ^c/4 ^d/2 f/4 ^A/2 z/2 ^c3/8 z/8 ^c/2 ^g
f ^d7/8 z/8 ^d/4 ^c/4 ^d/4 ^c/4 ^d/4 ^c/4 ^d/2 ^c/4 ^d/2 f/8 z/8 f/4
^d3/8 z7/8 ^c3/8 z/8 ^g f3/4 z/4 [^a9/8z] ^g ^f3/4 ^c/8 z/8 ^d/4 ^c/8
z/8 [^d5/8z/2] =f/8 z/8 [^f3/8z/4] [=f5/8z/2] ^c3/8 z/8 ^c/2 [^g9/8z]
[^d9/8z] ^c ^c/2 [^c7/8z5/8] [^gz7/8] [f25/8z3] ^d3/2 ^c9/8 z3/8
^c3/8 z/8 ^c/4 z/4 [^g9/8z] [f11/8z5/4] ^f/4 [=f13/8z3/2] ^d3/2 ^c3/4
z5/4 ^c/2 ^g f ^a ^g ^f5/8 z/8 ^c/4 ^d/4 ^c/4 [^d3/4z/2] =f/4
[^f/2z3/8] [=f5/8z3/8] ^c/4 z/4 ^c3/8 z/4 [^g9/8z] =c11/8 ^c/4
[=c3/8z/4] ^A27/8 